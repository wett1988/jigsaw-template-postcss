<?php

use Illuminate\Support\Arr;

$init->setup()
     ->delete('source/_assets/*')
     ->delete('source/assets/build/*')
     ->copy('webpack.mix.js')
     ->copy('source/*');

$package = json_decode(file_get_contents('package.json'), true);

$package['devDependencies'] = array_merge([
  'postcss-import' => '*',
  'postcss-nested' => '*',
  'tailwindcss' => '^1.0.6',
  'autoprefixer' => '*',
  '@fullhuman/postcss-purgecss' => '*',
  'cssnano' => '*'
], Arr::except($package['devDependencies'], [
  'sass',
  'sass-loader'
]));

$package['scripts'] = array_merge([
  'generate' => './vendor/bin/jigsaw build'
], $package['scripts']);

$fp = fopen('package.json', 'w');
fwrite($fp, json_encode($package, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
fclose($fp);

$init->output('Greate success!');
