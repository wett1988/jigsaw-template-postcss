/*
** TailwindCSS Configuration File
**
** Docs: https://next.tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',

      // black: '#212529',
      black: '#000',
      white: '#eeeeee',

      gray: {
        400: '#a5a7a5',
        300: '#efeeed'
      },

      red: {
        400: '#D9230F'
      },

      blue: {
        300: '#90cdf4',
        400: '#63b3ed'
      },
    },
    fontFamily: {
      sans: [
        'Open Sans',
        // '-apple-system',
        // 'BlinkMacSystemFont',
        // '"Segoe UI"',
        // 'Roboto',
        // '"Helvetica Neue"',
        // 'Arial',
        // '"Noto Sans"',
        // 'sans-serif',
        // '"Apple Color Emoji"',
        // '"Segoe UI Emoji"',
        // '"Segoe UI Symbol"',
        // '"Noto Color Emoji"',
        'sans-serif'
      ],
      serif: [
        'Merriweather',
        // 'Georgia',
        // 'Cambria',
        // '"Times New Roman"',
        // 'Times',
        'serif',
      ],
      mono: [
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace',
      ],
    }
  },
  variants: {},
  plugins: []
}
