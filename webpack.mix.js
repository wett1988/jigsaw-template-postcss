let mix = require('laravel-mix');
let build = require('./tasks/build.js');

mix.disableSuccessNotifications();
mix.setPublicPath('source/assets/build');
mix.webpackConfig({
    plugins: [
        build.jigsaw,
        build.browserSync(),
        // build.watch(['source/**/*.md', 'source/**/*.php', 'source/**/*.css', '!source/**/_tmp/*', '!source/assets/**/*']),
        build.watch(['source/**/*.md', 'source/**/*.php', 'source/_assets/**/*', '!source/**/_tmp/*', '!source/assets/**/*']),
    ]
});

const purgecss = require('@fullhuman/postcss-purgecss')({

  // Specify the paths to all of the template files in your project
  content: [
    './source/**/*.php',
    './source/**/*.vue',
    './source/**/*.md',
    // etc.
  ],

  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
})

mix.js('source/_assets/js/app.js', 'js')
   .postCss('source/_assets/postcss/app.css', 'css', [
     require('postcss-import'),
     require('tailwindcss')('source/_assets/postcss/tailwind.config.js'),
     require('autoprefixer'),
     require('postcss-nested'),
     // purgecss,
     require('cssnano')
   ])
   .options({
       processCssUrls: false,
   }).version();
